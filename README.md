# ttt
A simple Tic-Tac-Toe game written in C using Raylib.

You can customize the board length in cells and the length of the needed streak to win the game.

My second project in C.

## Compile & Run for Linux
```sh
git clone https://gitlab.com/sergeylavrent/ttt
cd ttt
make
```